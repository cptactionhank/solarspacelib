package dk.pmm.utilities;

public class Csv {
	public static final String csvRecord(Object... elements) {
		StringBuilder sb = new StringBuilder();
		// read all the entries
		for (int i = 0; i < elements.length; i++) {
			if (i > 0)
				sb.append(", ");
			sb.append(elements[i].toString());
		}
		return sb.toString();
	}
}
