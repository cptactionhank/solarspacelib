package dk.pmm.controls.pid;

import java.io.IOException;

import dk.pmm.messaging.Messenger;

public class RemotePidControl extends PidControl {

	private Messenger messenger;
	private int logDelay = 1000;

	public RemotePidControl(final int offset, final Messenger messenger) {
		super(offset);
		this.messenger = messenger;
	}

	@Override
	public void setOffset(int value) {
		sendcommand(ID_OFFSET, value);
	}

	@Override
	public void setPower(int value) {
		sendcommand(ID_POWER, value);
	}

	@Override
	public void setKp(float value) {
		sendcommand(ID_KP, value);
	}

	@Override
	public void setKi(float value) {
		sendcommand(ID_KI, value);
	}

	@Override
	public void setKd(float value) {
		sendcommand(ID_KD, value);
	}

	@Override
	public void setScale(float value) {
		sendcommand(ID_SCALE, value);
	}

	@Override
	public void report() {
		sendcommand(ID_REPORT, null);
	}
	
	@Override
	public void reset() {
		sendcommand(ID_RESET, null);
	}
	
	@Override
	public void log(int channel, Object... elements) {
		try {
			messenger.logThrottledInterval(channel, true, logDelay, elements);
		} catch (IOException e) {
			exception(e);
		}	
	}

	private final void sendcommand(int id, Object value) {
		try {
			messenger.vendorCommand(id, value);
		} catch (IOException e) {
			exception(e);
		}
	}
	
}
