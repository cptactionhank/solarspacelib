package dk.pmm.controls.pid;

import dk.pmm.messaging.events.MessagingEventObserver;

public abstract class PidControl extends MessagingEventObserver {

	protected final int ID_OFFSET;
	protected final int ID_POWER;
	protected final int ID_KP;
	protected final int ID_KI;
	protected final int ID_KD;
	protected final int ID_SCALE;
	protected final int ID_REPORT;
	protected final int ID_RESET;

	public PidControl(final int offset) {
		this.ID_OFFSET = offset + 0x00;
		this.ID_POWER = offset + 0x01;
		this.ID_KP = offset + 0x02;
		this.ID_KI = offset + 0x03;
		this.ID_KD = offset + 0x04;
		this.ID_SCALE = offset + 0x05;
		this.ID_REPORT = offset + 0x06;
		this.ID_RESET = offset + 0x07;
	}

	public abstract void setOffset(int value);

	public abstract void setPower(int value);

	public abstract void setKp(float value);

	public abstract void setKi(float value);

	public abstract void setKd(float value);

	public abstract void setScale(float value);

	public abstract void report();
	
	public abstract void reset();

	@Override
	protected void onVendorCommand(int identifier, Object value) {
		// ignore non relevant commands
		if (identifier >= ID_OFFSET && identifier <= ID_RESET) {
			// handle incoming commands
			if (identifier == ID_OFFSET) {
				setOffset((Integer) value);
			} else if (identifier == ID_POWER) {
				setPower((Integer) value);
			} else if (identifier == ID_KP) {
				setKp((Float) value);
			} else if (identifier == ID_KI) {
				setKi((Float) value);
			} else if (identifier == ID_KD) {
				setKd((Float) value);
			} else if (identifier == ID_SCALE) {
				setScale((Float) value);
			} else if (identifier == ID_REPORT) {
				report();
			} else if (identifier == ID_RESET) {
				reset();
			}
		}
	}

}