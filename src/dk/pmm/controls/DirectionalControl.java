package dk.pmm.controls;

import dk.pmm.messaging.events.MessagingEventObserver;

public abstract class DirectionalControl extends MessagingEventObserver {

	protected final int ID_NORTH;
	protected final int ID_SOUTH;
	protected final int ID_EAST;
	protected final int ID_WEST;
	protected final int ID_BOOST;

	public DirectionalControl(final int offset) {
		this.ID_NORTH	= offset;
		this.ID_SOUTH 	= offset + 1;
		this.ID_EAST	= offset + 2;
		this.ID_WEST 	= offset + 3;
		this.ID_BOOST 	= offset + 4;
	}

	public void north() {
		north(false);
	};

	public void north(boolean stop) {
	};

	public void south() {
		south(false);
	};

	public void south(boolean stop) {
	};

	public void east() {
		east(false);
	};

	public void east(boolean stop) {
	};

	public void west() {
		west(false);
	};

	public void west(boolean stop) {
	};

	public void boost(boolean stop) {

	}

	@Override
	protected void onVendorCommand(int identifier, Object value) {
		// see if we want to handle the command
		if (identifier >= ID_NORTH && identifier <= ID_BOOST) {
			// validate incoming arguments
			if (!(value instanceof Boolean))
				throw new IllegalArgumentException(
						"The argument passed should be of type Boolean, id:="
								+ identifier);
			// convert the object to boolean
			boolean stop = (Boolean) value;
			if (identifier == ID_NORTH)
				north(stop);
			else if (identifier == ID_SOUTH)
				south(stop);
			else if (identifier == ID_EAST)
				east(stop);
			else if (identifier == ID_WEST)
				west(stop);
			else if (identifier == ID_BOOST)
				boost(stop);
		}
	}

}