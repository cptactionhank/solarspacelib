package dk.pmm.controls;

import java.io.IOException;

import dk.pmm.messaging.Messenger;

public class RemoteDirectionalControl extends DirectionalControl {

	private final Messenger messenger;

	public RemoteDirectionalControl(int offset, Messenger messenger) {
		super(offset);
		this.messenger = messenger;
	}

	@Override
	public void north(boolean stop) {
		sendcommand(ID_NORTH, stop);
	}

	@Override
	public void south(boolean stop) {
		sendcommand(ID_SOUTH, stop);
	}

	@Override
	public void east(boolean stop) {
		sendcommand(ID_EAST, stop);
	}

	@Override
	public void west(boolean stop) {
		sendcommand(ID_WEST, stop);
	}

	@Override
	public void boost(boolean stop) {
		sendcommand(ID_BOOST, stop);
	}

	private final void sendcommand(int id, boolean stop) {
		try {
			messenger.vendorCommand(id, stop);
		} catch (IOException e) {
			exception(e);
		}
	}
}
