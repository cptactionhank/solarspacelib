package dk.pmm;

public final class StringConstants {
	
	public static final String CONNECTION_WAITING 	= "Waiting for connection";
	public static final String CONNECTION_SUCCESS 	= "Successfully connected";
	public static final String CONNECTION_CLOSING 	= "Closing connection";
	public static final String CONNECTION_CLOSED 	= "Closed connection";

}