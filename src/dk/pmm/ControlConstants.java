package dk.pmm;

public final class ControlConstants {

	public static final int OFFSET_REMOTE 	= 10;
	public static final int OFFSET_PID		= 20;
	public static final int CHANNEL_PUBLIC 	= 0b00000000;
	public static final int CHANNEL_PUBLIC1	= 0b00000001;
	public static final int CHANNEL_PUBLIC2	= 0b00000010;
	public static final int CHANNEL_PID 	= 0b00000011;

}
