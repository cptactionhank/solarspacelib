package dk.pmm.messaging.events;


public final class LogEvent {

	public final int channel;
	public final Object[] elements;

	public LogEvent(int channel, Object... elements) {
		this.channel = channel;
		this.elements = elements;
	}
}
