package dk.pmm.messaging.events;

import java.util.Vector;

public class MessagingEventObservable {

	private Vector<MessagingEventObserver> obs;

	public MessagingEventObservable() {
		obs = new Vector<MessagingEventObserver>();
	}

	public synchronized void addObserver(MessagingEventObserver o) {
		if (o == null)
			throw new NullPointerException();
		// Contains yields linker null pointer exception, :@
		// if (!obs.contains(o)) {
		// obs.addElement(o);
		// }
		// this works instead
		
		if (obs.indexOf(o, 0) < 0) {
			obs.addElement(o);
		}
	}

	public synchronized void deleteObserver(MessagingEventObserver o) {
		obs.removeElement(o);
	}

	public void notifyObservers() {
		notifyObservers(null);
	}

	public void notifyObservers(Object arg) {

		Object[] arrLocal;

		synchronized (this) {
			arrLocal = obs.toArray();
		}

		for (int i = arrLocal.length - 1; i >= 0; i--) {
			((MessagingEventObserver) arrLocal[i]).update(this, arg);
		}
	}

	public synchronized void deleteObservers() {
		 obs.removeAllElements();
	}

	public synchronized int countObservers() {
		return obs.size();
	}
}
