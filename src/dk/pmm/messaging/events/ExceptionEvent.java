package dk.pmm.messaging.events;

public class ExceptionEvent {

	public final Throwable exception;
	
	public ExceptionEvent(Throwable exception) {
		this.exception = exception;
	}
	
}
