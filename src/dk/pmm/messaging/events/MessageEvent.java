package dk.pmm.messaging.events;

public class MessageEvent {

	public final String message;

	public MessageEvent(String message) {
		this.message = message;
	}

}
