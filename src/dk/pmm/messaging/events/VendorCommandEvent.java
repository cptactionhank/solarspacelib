package dk.pmm.messaging.events;

public class VendorCommandEvent {

	public final int command;
	public final Object argument;

	public VendorCommandEvent(int command, Object argument) {
		this.command = command;
		this.argument = argument;
	}
	
	@Override
	public String toString() {
		return "CMD: " + command + " VAL: " + argument;
	}

}
