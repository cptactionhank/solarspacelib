package dk.pmm.messaging.events;

import dk.pmm.Base;

public abstract class MessagingEventObserver extends Base {

	public void update(MessagingEventObservable  o, Object arg) {
		if (arg instanceof LogEvent) {
			LogEvent event = (LogEvent) arg;
			log(event.channel, event.elements);
		} else if (arg instanceof MessageEvent) {
			onMessageCommand(((MessageEvent)arg).message);
		} else if (arg instanceof VendorCommandEvent) {
			VendorCommandEvent cmd = (VendorCommandEvent)arg;
			onVendorCommand(cmd.command, cmd.argument);
		} else if (arg instanceof ExceptionEvent) {
			exception(((ExceptionEvent)arg).exception);
		}
	}
	
	@Override
	public void log(int channel, Object... elements) {
		// do nothing
	}
	
	@Override
	public void exception(Thread t, Throwable e) {
		// do nothing
	}
	
	@Override
	protected void onMessageCommand(String message) {
		// do nothing
	}
	
	@Override
	protected void onVendorCommand(int identifier, Object object) {
		// do nothing
	}
	
}
