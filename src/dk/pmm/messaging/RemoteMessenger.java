package dk.pmm.messaging;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import dk.pmm.messaging.events.LogEvent;
import dk.pmm.messaging.events.MessageEvent;
import dk.pmm.messaging.events.MessagingEventObservable;
import dk.pmm.messaging.events.MessagingEventObserver;
import dk.pmm.messaging.events.VendorCommandEvent;

public class RemoteMessenger implements Messenger {

	// default the log throttle at 200ms or 5 messages per second
	private static final int defaultThrottle = 200;
	// threading lock object
	private final Object LOCK = new Object();
	private final MessagingEventObservable observable = new MessagingEventObservable();

	// state variables
	private DataInputStream inputStream = null;
	private DataOutputStream outputStream = null;
	private long[] lastLog;

	public RemoteMessenger(InputStream inputStream, OutputStream outputStream) {
		this.inputStream = new DataInputStream(inputStream);
		this.outputStream = new DataOutputStream(new BufferedOutputStream(
				outputStream));
		// initialize channel timestamps
		lastLog = new long[MessagingConstants.MAX_LOG_CHANNELS];
		for (int i = 0; i < lastLog.length; i += 1) {
			lastLog[i] = 0;
		}
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#listen()
	 */
	@Override
	public void listen() throws Exception {
		while (!Thread.interrupted()) {
			// message
			byte message_header = (byte) inputStream.read();
			// quit if there is no more to read
			if (message_header < 0)
				return;
			// command or request
			// boolean is_command = (message_header &
			// MessagingConstants.MASK_MESSAGE_CMD) ==
			// MessagingConstants.MESSAGE_COMMAND;
			// boolean is_request = (message_header &
			// MessagingConstants.MASK_MESSAGE_CMD) ==
			// MessagingConstants.MESSAGE_REQUEST;
			// command type
			int message_type = (message_header & MessagingConstants.MASK_MESSAGE_TYPE);
			// command identifier
			int command_id = (message_header & MessagingConstants.MASK_CMD_ID);
			// handle message types
			if (message_type == MessagingConstants.COMMAND_LOG)
				handleLog(command_id);
			else if (message_type == MessagingConstants.COMMAND_VENDOR)
				handleVendorCommand(command_id);
			else if (message_type == MessagingConstants.COMMAND_SYSTEM)
				handleSystemCommand(command_id);
			else
				throw new RuntimeException(
						"No command handler available for header:"
								+ Integer.toHexString(message_header)
								+ " type: " + Integer.toHexString(message_type)
								+ " identifier: "
								+ Integer.toHexString(command_id));
		}
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#log(int, java.lang.Object)
	 */
	@Override
	public synchronized void log(int channel, Object... args)
			throws IOException {
		logThrottled(channel, true, args);
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#logThrottled(int, boolean, java.lang.Object)
	 */
	
	public synchronized void logThrottled(int channel, boolean throttle,
			Object... args) throws IOException {
		logThrottledInterval(channel, throttle, defaultThrottle, args);
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#logThrottledInterval(int, boolean, int, java.lang.Object)
	 */
	
	public synchronized void logThrottledInterval(int channel,
			boolean throttle, int throttleInterval, Object... args)
			throws IOException {
		if (channel > MessagingConstants.MAX_LOG_CHANNELS)
			throw new ArrayIndexOutOfBoundsException("channel identifier is too large, " + channel);
		if (throttle) {
			long currentTime = System.currentTimeMillis();
			if (currentTime - this.lastLog[channel] >= throttleInterval)
				this.lastLog[channel] = currentTime;
			else
				return;
		}
		int entries = Math.min(1 + args.length,
				MessagingConstants.MAX_LOG_ENTRIES);
		int header = MessagingConstants.MESSAGE_COMMAND
				| MessagingConstants.COMMAND_LOG
				| ((channel << MessagingConstants.SHIFT_LOG_CHANNEL) & MessagingConstants.MASK_LOG_CHANNEL) 
				| ((entries << MessagingConstants.SHIFT_LOG_ENTRIES) & MessagingConstants.MASK_CMD_ID);
		// write out the message header
		outputStream.write(header);
		outputStream.flush();
		// write mandatory time stamp
		writeDataEntry(outputStream, System.currentTimeMillis());
		outputStream.flush();
		// write out the log entries
		for (int i = 0; i < entries - 1; i++) {
			writeDataEntry(outputStream, args[i]);
			outputStream.flush();
		}
		outputStream.flush();
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#vendorCommand(int, java.lang.Object)
	 */
	@Override
	public final synchronized void vendorCommand(int id, Object value)
			throws IOException {
		if (id > MessagingConstants.MASK_CMD_ID) 
			throw new IllegalArgumentException("Too large command id: " + id);
		int header = MessagingConstants.MESSAGE_COMMAND
				| MessagingConstants.COMMAND_VENDOR
				| (id & MessagingConstants.MASK_CMD_ID);
		outputStream.write(header);
		writeDataEntry(outputStream, value);
		outputStream.flush();
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#kill()
	 */
	@Override
	public final synchronized void kill() throws IOException {
		int header = MessagingConstants.MESSAGE_COMMAND
				| MessagingConstants.COMMAND_SYSTEM
				| MessagingConstants.COMMAND_SYSTEM_KILL;
		outputStream.write(header);
		outputStream.flush();
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#message(java.lang.String)
	 */
	@Override
	public final synchronized void message(String string) throws IOException {
		int header = MessagingConstants.MESSAGE_COMMAND
				| MessagingConstants.COMMAND_SYSTEM
				| MessagingConstants.COMMAND_SYSTEM_MSG;
		outputStream.write(header);
		outputStream.writeUTF(string);
		outputStream.flush();
	}

	private void handleLog(int cmd) throws IOException {
		int channel = (cmd & MessagingConstants.MASK_LOG_CHANNEL) >> MessagingConstants.SHIFT_LOG_CHANNEL;
		int entries = (cmd & MessagingConstants.MASK_LOG_ENTRIES) >> MessagingConstants.SHIFT_LOG_ENTRIES;
		// create array of elements
		Object[] elements = new Object[entries];
		// lock to fix bugs
		synchronized (LOCK) {
			;
			for (int i = 0; i < entries; i++)
				elements[i] = readDataEntry(inputStream);
		}
		// handle logging
		observable.notifyObservers(new LogEvent(channel, elements));
	}

	private void handleSystemCommand(int identifier) throws IOException {
		if (MessagingConstants.COMMAND_SYSTEM_KILL == identifier)
			onKillCommand();
		else if (MessagingConstants.COMMAND_SYSTEM_MSG == identifier) {
			observable.notifyObservers(new MessageEvent(inputStream.readUTF()));
		}
	}

	private void handleVendorCommand(int identifier) throws IOException {
		Object value;
		// read the command value
		synchronized (LOCK) {
			value = readDataEntry(inputStream);
		}
		// handle simple command event
		observable.notifyObservers(new VendorCommandEvent(identifier, value));
	}

	protected void onKillCommand() {
		// do nothing
	}

	public static final void writeDataEntry(DataOutputStream outputStream,
			Object object) throws IOException {
		if (object == null) {
			outputStream.write(MessagingConstants.TYPE_NULL);
		} else if (object instanceof String) {
			outputStream.write(MessagingConstants.TYPE_STRING);
			outputStream.writeUTF((String) object);
		} else if (object instanceof Integer) {
			outputStream.write(MessagingConstants.TYPE_INTEGER);
			outputStream.writeInt((Integer) object);
		} else if (object instanceof Float) {
			outputStream.write(MessagingConstants.TYPE_FLOAT);
			outputStream.writeFloat((Float) object);
		} else if (object instanceof Long) {
			outputStream.write(MessagingConstants.TYPE_LONG);
			outputStream.writeLong((Long) object);
		} else if (object instanceof Double) {
			outputStream.write(MessagingConstants.TYPE_DOUBLE);
			outputStream.writeDouble((Double) object);
		} else if (object instanceof Boolean) {
			outputStream.write(MessagingConstants.TYPE_BOOLEAN);
			outputStream.writeBoolean((Boolean) object);
		} else if (object.getClass().isArray()) {
			Object[] arr = (Object[]) object;
			outputStream.write(MessagingConstants.TYPE_ARRAY);
			outputStream.writeByte(arr.length);
			for (Object value : arr)
				writeDataEntry(outputStream, value);
		} else {
			// default for unsupported types
			outputStream.write(MessagingConstants.TYPE_STRING);
			outputStream.writeUTF(object.toString());
		}
	}

	public static final Object readDataEntry(DataInputStream inputStream)
			throws IOException {
		// read the data type
		byte data_type = (byte) inputStream.read();
		// read the data content
		if (data_type == MessagingConstants.TYPE_NULL) {
			return null;
		} else if (data_type == MessagingConstants.TYPE_STRING) {
			return inputStream.readUTF();
		} else if (data_type == MessagingConstants.TYPE_INTEGER) {
			return inputStream.readInt();
		} else if (data_type == MessagingConstants.TYPE_FLOAT) {
			return inputStream.readFloat();
		} else if (data_type == MessagingConstants.TYPE_LONG) {
			return inputStream.readLong();
		} else if (data_type == MessagingConstants.TYPE_DOUBLE) {
			return inputStream.readDouble();
		} else if (data_type == MessagingConstants.TYPE_BOOLEAN) {
			return inputStream.readBoolean();
		} else if (data_type == MessagingConstants.TYPE_ARRAY) {
			Object[] arr = new Object[inputStream.readByte()];
			for (int i = 0; i < arr.length; i++)
				arr[i] = readDataEntry(inputStream);
			return arr;
		} else {
			throw new RuntimeException("Unsupported datatype 0x"
					+ Integer.toHexString(data_type));
		}
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#addObserver(dk.pmm.messaging.events.MessagingEventObserver)
	 */
	@Override
	public void addObserver(MessagingEventObserver observer) {
		observable.addObserver(observer);
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#removeObserver(dk.pmm.messaging.events.MessagingEventObserver)
	 */
	@Override
	public void removeObserver(MessagingEventObserver observer) {
		observable.deleteObserver(observer);
	}

	/* (non-Javadoc)
	 * @see dk.pmm.messaging.Messenger#removeObservers()
	 */
	@Override
	public void removeObservers() {
		observable.deleteObservers();
	}

}
