package dk.pmm.messaging;

import java.io.IOException;

import dk.pmm.messaging.events.MessagingEventObserver;

public interface Messenger {

	public abstract void listen() throws Exception;

	public abstract void log(int channel, Object... args) throws IOException;

	public abstract void logThrottled(int channel, boolean throttle,
			Object... args) throws IOException;

	public abstract void logThrottledInterval(int channel, boolean throttle,
			int throttleInterval, Object... args) throws IOException;

	public abstract void vendorCommand(int id, Object value) throws IOException;

	public abstract void kill() throws IOException;

	public abstract void message(String string) throws IOException;

	public abstract void addObserver(MessagingEventObserver observer);

	public abstract void removeObserver(MessagingEventObserver observer);

	public abstract void removeObservers();

}