package dk.pmm.messaging;

public class MessagingConstants {
	
	public final static int MASK_MESSAGE_CMD	= 0b10000000;
	public final static int MESSAGE_COMMAND		= 0b00000000;
	public final static int MESSAGE_REQUEST		= 0b10000000;
	
	public final static int MASK_MESSAGE_TYPE	= 0b01100000;
	public static final int COMMAND_LOG 		= 0b00000000;
	public static final int COMMAND_VENDOR 		= 0b00100000;
	public static final int COMMAND_SYSTEM		= 0b01000000;
	public static final int COMMAND_RESERVED 	= 0b01100000;
	
	public final static int MASK_CMD_ID 		= 0b00011111;
	public final static int MASK_LOG_ENTRIES    = 0b00000111;
	public final static int MASK_LOG_CHANNEL	= 0b00011000;
	public final static int SHIFT_LOG_CHANNEL	= 3;
	public final static int SHIFT_LOG_ENTRIES 	= 0;
	public final static int MAX_LOG_ENTRIES 	= 7;
	public final static int MAX_LOG_CHANNELS	= 4;

	public static final int COMMAND_SYSTEM_KILL = 0b00000000 & MASK_CMD_ID;
	public static final int COMMAND_SYSTEM_MSG  = 0b00000001 & MASK_CMD_ID;
	
	public final static int TYPE_NULL 			= 0x00;
	public final static int TYPE_STRING 		= 0x01;
	public final static int TYPE_INTEGER 		= 0x02;
	public final static int TYPE_FLOAT 			= 0x03;
	public final static int TYPE_LONG 			= 0x04;
	public final static int TYPE_DOUBLE 		= 0x05;
	public final static int TYPE_BOOLEAN		= 0x06;
	public final static int TYPE_ARRAY			= 0x07;
	
}
