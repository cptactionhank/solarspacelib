package dk.pmm.normalization;

public class LightNormalizer {

	public static float normalize(int raw, float lower, float upper) {
		return raw * (upper - lower) / 1023;
	}

	public static int normalizeDark(int rawLight, int lowerBound, int upperBound) {
		return -1;
	}

	public static int normalizeLight(int rawLight, int lowerBound,
			int upperBound) {
		return -1;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 1024; i++) {
			System.out.println(normalize(i, 0, 100));
		}
	}

}
