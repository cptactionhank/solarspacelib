package dk.pmm;

import java.lang.Thread.UncaughtExceptionHandler;

public abstract class Base {

	protected int logDelay = 100;

	public Base setup() {
		// assume this is run from main thread
		Thread.currentThread().setName("Main");
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {

			@Override
			public void uncaughtException(Thread t, Throwable e) {
				exception(t, e);
			}
		});
		// return this instance for fluent interfacing
		return this;
	}

	
	protected void onVendorCommand(int identifier, Object object) {
		// do nothing
	}

	protected void onMessageCommand(String message) {
		// do nothing
	}
	
	protected void log(int channel, Object...elements) {
		// do nothing
	}

	public void exception(Throwable e) {
		exception(Thread.currentThread(), e);
	}

	public void exception(Thread t, Throwable e) {
		// just go to hard exception
		hardException(t, e);
	}

	protected final static void hardException(Thread t, Throwable e) {
		// print the exception on the screen
		System.err.println("Thread: " + t.getName());
		e.printStackTrace();
		System.err.flush();
	}
}
